#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#define ROZMIAR 8192

int main(int liczba_argumentow, char *tablica_argumentow[])
{
    int deskryptor_pliku;
    char tablica_na_pobrane_z_pliku_litery[ROZMIAR];
    char obrocona_tablica[ROZMIAR];
    int ilosc_przeczytanych_bajtow;

    if (liczba_argumentow < 2)
    {
        fprintf(stderr, "Za malo argumentow\nZrob: %s <nazwa pliku>\n", tablica_argumentow[0]);
    }
    deskryptor_pliku = open(tablica_argumentow[1], O_RDWR);

    int licznik = 0;
    while ((ilosc_przeczytanych_bajtow = read(deskryptor_pliku, tablica_na_pobrane_z_pliku_litery, ROZMIAR)) > 0)
    {
        lseek(deskryptor_pliku, -ilosc_przeczytanych_bajtow, SEEK_CUR);

        int licznik = 0;
        for (int i = 0; i < ilosc_przeczytanych_bajtow; i++)
        {
            char litera = tablica_na_pobrane_z_pliku_litery[i];
            if (isalpha(litera) != 0)
            {
                licznik++;
            }
            // TODO: CO JESLI LINIA NIE KONCZY SIE ZNAKIEM
            // INNYM NIZ ALFANUMERYCZNY
            else
            {
                int t = i - 1;
                for (int j = 0; j < licznik; j++)
                {
                    obrocona_tablica[j] = tablica_na_pobrane_z_pliku_litery[t];
                    t--;
                }
                // TODO: CO JESLI LITERA TO NIE \n
                obrocona_tablica[licznik] = litera;

                write(deskryptor_pliku, obrocona_tablica, licznik + 1);

                licznik = 0;
            }
        }
    }
}